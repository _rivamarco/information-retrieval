# train word2vec with train dataset


from gensim.models import word2vec
from time import time
import json
from pymongo import MongoClient
import pymongo
import spacy
import re

with open('word2vec/train_word2vec_80k_three_1mln.json') as recipes_file:
	train = json.load(recipes_file)

# Initialize and train the model 
model = word2vec.Word2Vec(min_count=200,
                     window=20,
                     size=400,
                     sample=0e-5, 
                     alpha=0.04, 
                     min_alpha=0.0001, 
                     negative=15,
                     workers=2, iter=15)

t = time()

model.build_vocab(train, progress_per=10000)

print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))


t = time()

print(model.corpus_count)

model.train(train, total_examples=model.corpus_count, epochs=30, report_delay=1)

model.init_sims(replace=True)

model.save("1mln_80k_new_less.model")

print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))

print('MOST SIMILAR')

print(model.wv.similar_by_word('eggs'))

print(model.wv.most_similar('eggs'))
print(model.wv.most_similar('oranges'))
print(model.wv.most_similar('mushrooms'))
print(model.wv.most_similar('fresh strawberries'))
print(model.wv.most_similar('parmesan cheese'))


print('SIMILARITY')

print(model.wv.similarity('lemon juice', 'meat'))
print(model.wv.similarity('honey', 'sugar'))
print(model.wv.similarity('garlic cloves', 'garlic'))
print(model.wv.similarity('mozzarella cheese', 'cheddar cheese'))
print(model.wv.similarity('sugar', 'eggs'))


print('RECIPES')


recipe1 = ['spaghetti', 'garlic', 'pepper', 'salt', 'parmigiano', 'tomatoes']
recipe2 = ['flour', 'eggs', 'chocolate', 'cinnamon', 'orange juice']
recipe3  = ['linguine', 'tomatoes', 'salt', 'basil', 'pepper']
recipe4  = ['butter', 'garlic', 'milk', 'heavy cream']

print(model.wv.n_similarity(recipe1, recipe2))
print(model.wv.n_similarity(recipe1, recipe3))
print(model.wv.n_similarity(recipe2, recipe3))


print(model.wv.most_similar(positive = recipe1, topn = 10))
print(model.wv.most_similar(positive = recipe2, topn = 10))
print(model.wv.most_similar(positive = recipe3, topn = 10))
print(model.wv.most_similar(positive = recipe4, topn = 10))

print(model.predict_output_word(recipe1, topn=10))
print(model.predict_output_word(recipe2, topn=10))

print(model.most_similar_cosmul(positive = recipe1, topn = 1))

host = 'localhost'
port = 27017

myclient = MongoClient(host, port)
db = myclient["recipesdb"]
recipes = db["new_recipes"]

pymongo_cursor = db.recipes.find().skip(800000).limit(10)

all_r = []

for recipe in pymongo_cursor:
	recipe_ingredients = []
	for recipe_ingredient in recipe['ingredients']:
		recipe_ingredients.append(recipe_ingredient['text'])
	all_r.append(recipe_ingredients)
nlp = spacy.load('models/60k_three/model-best')

all_recipes = []

for ingredients in all_r:
	recipe_ingredients = set()
	for ingredient in ingredients:
		ingredient = ingredient.lower()
		ingredient = re.sub(r"[^a-zA-Z0-9]+", ' ', ingredient).strip()
		ingredient = re.sub(r'\b\w{1,2}\b', ' ', ingredient).strip()
		ingredient = ' '.join(ingredient.split())
		if ingredient != '' or ingredient != ' ':
			recipe_ingredients.add(ingredient)
	all_recipes.append(recipe_ingredients)


predict = []

for i, recipe in enumerate(all_recipes):
	test_ingredients = set()
	for ingredient in recipe:
		doc = nlp(ingredient)
		for ent in doc.ents:
			test_ingredients.add(ent.text)
	predict.append(list(test_ingredients))
 
predict_minus_one = list(map(lambda x: x[:-1], predict))

print(predict_minus_one)
print(predict)

for recipe,r in zip(predict_minus_one, predict):
	try:
		missing = set(r).difference(set(recipe))
		print("\n\n\n\n")
		print(missing)
		print(model.wv.most_similar(positive = recipe, topn = 15))
		print(model.predict_output_word(recipe, topn=15))
	except KeyError:
		pass

	