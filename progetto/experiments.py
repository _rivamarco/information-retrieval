# print(model['eggs'])

# from sklearn.cluster import DBSCAN
# dbscan = DBSCAN(eps=0.81, min_samples=3) # you can change these parameters, given just for example
# cluster_labels = dbscan.fit_predict(X) # where X - is your matrix, where each row corresponds to one document (line) from the docs, you need to cluster
#
# from collections import Counter
#
# print(cluster_labels)
#
# print(list(zip(Counter(cluster_labels).keys(), Counter(cluster_labels).values())))
#
# print([f"{x}, {y}" for y,x in sorted(zip(cluster_labels,vocab))])
# print([f"{v}, {label}" for v,label in zip(vocab, cluster_labels) if label == 1])
#
# print(zip(vocab, cluster_labels))

# import pandas as pd
#
# df = pd.DataFrame(X, index=zip(vocab, cluster_labels))
#
# print(df)

# from sklearn.neighbors import NearestNeighbors
#
# import matplotlib.pyplot as plt
#
# neigh = NearestNeighbors(n_neighbors=2)
# nbrs = neigh.fit(X)
# distances, indices = nbrs.kneighbors(X)
#
# distances = np.sort(distances, axis=0)
# distances = distances[:,1]
# plt.plot(distances)
#
# plt.show()


# from nltk.cluster import KMeansClusterer
# import nltk
# NUM_CLUSTERS=50
# kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance, repeats=25)
# assigned_clusters = kclusterer.cluster(X, assign_clusters=True)
#
# print(assigned_clusters)
#
# for i, word in enumerate(vocab[:200]):
#     print (word + ":" + str(assigned_clusters[i]))


# from sklearn.cluster import AffinityPropagation
# from sklearn import metrics
#
# # Compute Affinity Propagation
# af = AffinityPropagation(preference=-20).fit(X)
# cluster_centers_indices = af.cluster_centers_indices_
# labels = af.labels_
#
# n_clusters_ = len(cluster_centers_indices)
#
# print('Estimated number of clusters: %d' % n_clusters_)
#
# print(labels)
#
# print(af.predict([model['eggs'], model['egg'], model['egg yolk'], model['egg whites'], model['orange'], model['oranges'], model['navel oranges']]))
