 rivamarco  (e) .env  ~  inforet_new  spacy train en models/20k_three training_data/converted_20k_three_ingredients_train.json training_data/converted_20k_three_ingredients_validation.json -p ner 
Training pipeline: ['ner']
Starting with blank model 'en'
Counting training words (limit=0)

Itn  NER Loss   NER P   NER R   NER F   Token %  CPU WPS
---  ---------  ------  ------  ------  -------  -------
  1  47796.878  95.677  94.922  95.298  100.000    16849                                                                                                                                       
  2  27540.280  96.824  96.275  96.548  100.000    16463                                                                                                                                       
  3  23045.005  97.346  96.955  97.150  100.000    16424                                                                                                                                       
  4  20211.454  97.672  97.384  97.528  100.000    16181                                                                                                                                       
  5  17762.002  97.984  97.707  97.845  100.000    16335                                                                                                                                       
  6  16336.086  98.202  97.945  98.073  100.000    16426                                                                                                                                       
  7  14994.509  98.429  98.257  98.343  100.000    16376                                                                                                                                       
  8  14018.905  98.601  98.432  98.517  100.000    16409                                                                                                                                       
  9  12954.769  98.753  98.607  98.680  100.000    16348                                                                                                                                       
 10  12107.137  98.931  98.766  98.848  100.000    16459                                                                                                                                       
 11  11466.120  99.040  98.900  98.970  100.000    16229                                                                                                                                       
 12  10843.498  99.116  98.997  99.057  100.000    16010                                                                                                                                       
 13  10501.984  99.168  99.054  99.111  100.000    16270                                                                                                                                       
 14   9835.228  99.189  99.095  99.142  100.000    16300                                                                                                                                       
 15   9507.107  99.253  99.180  99.217  100.000    16335                                                                                                                                       
 16   8997.769  99.321  99.250  99.285  100.000    16256                                                                                                                                       
 17   8723.976  99.374  99.304  99.339  100.000    16477                                                                                                                                       
 18   8505.176  99.404  99.341  99.372  100.000    16280                                                                                                                                       
 19   8296.353  99.426  99.372  99.399  100.000    16422                                                                                                                                       
 20   7865.082  99.470  99.415  99.443  100.000    16369                                                                                                                                       
 21   7739.337  99.491  99.433  99.462  100.000    16299                                                                                                                                       
 22   7564.258  99.558  99.509  99.533  100.000    16122                                                                                                                                       
 23   7223.057  99.595  99.558  99.576  100.000    16291                                                                                                                                       
 24   6985.440  99.631  99.593  99.612  100.000    16392                                                                                                                                       
 25   6829.652  99.642  99.600  99.621  100.000    16397                                                                                                                                       
 26   6628.268  99.676  99.636  99.656  100.000    16301                                                                                                                                       
 27   6354.393  99.687  99.645  99.666  100.000    16329                                                                                                                                       
 28   6171.153  99.710  99.676  99.693  100.000    16359                                                                                                                                       
 29   6243.984  99.744  99.718  99.731  100.000    16317                                                                                                                                       
 30   6262.701  99.759  99.736  99.747  100.000    16319                                                                                                                                       
✔ Saved model to output directory
models/20k_three/model-final
✔ Created best model
models/20k_three/model-best
 rivamarco  (e) .env  ~  inforet_new  python mongo_train.py 60000 60k_three_ingredients 60k_three_ingredientsMissing arguments: n filename
 rivamarco  (e) .env  ~  inforet_new  python mongo_train.py 60000 60k_three_ingredients                      
[nltk_data] Downloading package stopwords to
[nltk_data]     /home/rivamarco/nltk_data...
[nltk_data]   Package stopwords is already up-to-date!
12766
12766
 rivamarco  (e) .env  ~  inforet_new  python converter.py 60k_three_ingredients       
 rivamarco  (e) .env  ~  inforet_new  spacy train en models/60k_three training_data/converted_60k_three_ingredients_train.json training_data/converted_60k_three_ingredients_validation.json -p ner
Training pipeline: ['ner']
Starting with blank model 'en'
Counting training words (limit=0)

Itn  NER Loss   NER P   NER R   NER F   Token %  CPU WPS
---  ---------  ------  ------  ------  -------  -------
  1  100988.556  96.686  96.224  96.454  100.000    16490                                                                                                                                      
  2  60808.290  97.578  97.288  97.433  100.000    16371                                                                                                                                       
  3  49419.284  98.058  97.802  97.930  100.000    16457                                                                                                                                       
  4  43083.216  98.388  98.202  98.295  100.000    16430                                                                                                                                       
  5  38212.958  98.632  98.479  98.556  100.000    16233                                                                                                                                       
  6  34852.862  98.787  98.668  98.727  100.000    16241                                                                                                                                       
  7  32036.463  98.902  98.794  98.848  100.000    15618                                                                                                                                       
  8  29953.177  99.043  98.930  98.986  100.000    16272                                                                                                                                       
  9  28205.465  99.135  99.034  99.084  100.000    14162                                                                                                                                       
 10  26558.724  99.230  99.118  99.174  100.000    16140                                                                                                                                       
 11  25382.884  99.271  99.176  99.223  100.000    16099                                                                                                                                       
 12  24127.279  99.327  99.243  99.285  100.000    15957                                                                                                                                       
 13  23130.792  99.384  99.306  99.345  100.000    16139                                                                                                                                       
 14  22508.648  99.415  99.343  99.379  100.000    16076                                                                                                                                       
 15  21679.387  99.457  99.385  99.421  100.000    16084                                                                                                                                       
 16  20699.663  99.496  99.433  99.465  100.000    15981                                                                                                                                       
 17  20155.143  99.525  99.460  99.492  100.000    15912                                                                                                                                       
 18  19533.567  99.557  99.495  99.526  100.000    15791                                                                                                                                       
 19  18837.119  99.585  99.524  99.555  100.000    16153                                                                                                                                       
 20  18550.037  99.599  99.544  99.571  100.000    15966                                                                                                                                       
 21  18244.999  99.632  99.585  99.608  100.000    16066                                                                                                                                       
 22  17945.322  99.646  99.599  99.623  100.000    15847                                                                                                                                       
 23  17695.513  99.656  99.610  99.633  100.000    16061                                                                                                                                       
 24  17043.590  99.671  99.632  99.651  100.000    16066                                                                                                                                       
 25  17087.081  99.693  99.662  99.677  100.000    16014                                                                                                                                       
 26  16532.343  99.704  99.678  99.691  100.000    15916                                                                                                                                       
 27  16376.997  99.717  99.688  99.702  100.000    16035                                                                                                                                       
 28  16378.958  99.719  99.687  99.703  100.000    15972                                                                                                                                       
 29  16021.817  99.724  99.694  99.709  100.000    16025                                                                                                                                       
 30  15898.981  99.731  99.697  99.714  100.000    15778                                                                                                                                       
✔ Saved model to output directory
models/60k_three/model-final
✔ Created best model
models/60k_three/model-best
 rivamarco  (e) .env  ~  inforet_new  python mongo_train.py 60000 60k_two_ingredients           [nltk_data] Downloading package stopwords to
[nltk_data]     /home/rivamarco/nltk_data...
[nltk_data]   Package stopwords is already up-to-date!
Traceback (most recent call last):
  File "mongo_train.py", line 28, in <module>
    with open('ingredient_train_two_three.json') as json_file:
FileNotFoundError: [Errno 2] No such file or directory: 'ingredient_train_two_three.json'
 rivamarco  (e) .env  ~  inforet_new  python mongo_train.py 60000 60k_two_ingredients                                                                                                  1 
[nltk_data] Downloading package stopwords to
[nltk_data]     /home/rivamarco/nltk_data...
[nltk_data]   Package stopwords is already up-to-date!
7923
7923
 rivamarco  (e) .env  ~  inforet_new  python converter.py 60k_two_ingredients
 rivamarco  (e) .env  ~  inforet_new  spacy train en models/60k_two training_data/converted_60k_two_ingredients_train.json training_data/converted_60k_two_ingredients_validation.json -p ner
Training pipeline: ['ner']
Starting with blank model 'en'
Counting training words (limit=0)

Itn  NER Loss   NER P   NER R   NER F   Token %  CPU WPS
---  ---------  ------  ------  ------  -------  -------
  1  72330.092  98.076  97.888  97.982  100.000    16386                                                                                                                                       
  2  35982.198  98.746  98.628  98.687  100.000    15955                                                                                                                                       
  3  27910.159  99.057  98.965  99.011  100.000    16396                                                                                                                                       
  4  23601.691  99.257  99.176  99.217  100.000    15790                                                                                                                                       
  5  20443.686  99.369  99.315  99.342  100.000    16148                                                                                                                                       
  6  18450.079  99.447  99.396  99.421  100.000    15519                                                                                                                                       
  7  16812.857  99.514  99.468  99.491  100.000    16048                                                                                                                                       
  8  15756.269  99.576  99.542  99.559  100.000    16263                                                                                                                                       
  9  15059.942  99.609  99.580  99.595  100.000    16053                                                                                                                                       
 10  14280.936  99.661  99.638  99.650  100.000    16277                                                                                                                                       
 11  13573.460  99.680  99.660  99.670  100.000    16280                                                                                                                                       
 12  13142.328  99.716  99.699  99.707  100.000    16217                                                                                                                                       
 13  12454.810  99.744  99.729  99.737  100.000    16210                                                                                                                                       
 14  11814.123  99.759  99.740  99.749  100.000    16204                                                                                                                                       
 15  11665.765  99.780  99.767  99.773  100.000    15651                                                                                                                                       
 16  11355.661  99.801  99.790  99.796  100.000    16028                                                                                                                                       
 17  10637.010  99.809  99.800  99.805  100.000    16109                                                                                                                                       
 18  10602.046  99.824  99.816  99.820  100.000    16154                                                                                                                                       
 19  10370.604  99.840  99.830  99.835  100.000    16076                                                                                                                                       
 20  10115.996  99.856  99.850  99.853  100.000    16132                                                                                                                                       
 21   9746.402  99.865  99.856  99.860  100.000    15592                                                                                                                                       
 22   9560.440  99.867  99.862  99.865  100.000    16185                                                                                                                                       
 23   9414.723  99.872  99.864  99.868  100.000    15662                                                                                                                                       
 24   9325.289  99.877  99.871  99.874  100.000    15648                                                                                                                                       
 25   9087.632  99.880  99.871  99.876  100.000    15964                                                                                                                                       
 26   9010.870  99.881  99.871  99.876  100.000    16140                                                                                                                                       
 27   8597.154  99.882  99.872  99.877  100.000    16142                                                                                                                                       
 28   8721.250  99.894  99.882  99.888  100.000    16128                                                                                                                                       
 29   8710.498  99.899  99.889  99.894  100.000    15973                                                                                                                                       
 30   8406.890  99.894  99.887  99.891  100.000    16155                                                                                                                                       
✔ Saved model to output directory
models/60k_two/model-final
✔ Created best model
models/60k_two/model-best