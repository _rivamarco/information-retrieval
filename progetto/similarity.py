# Some similarity for project paper


import gensim

model = gensim.models.Word2Vec.load("1mln_80k_new_less.model")

print(model.wv.similar_by_word("eggs", topn=14))
print(model.wv.similar_by_word("oranges", topn=14))
print(model.wv.similar_by_word("olive oil", topn=14))
print(model.wv.similar_by_word("butter", topn=14))
print(model.wv.similar_by_word('fresh strawberries', topn=14))
print(model.wv.similar_by_word('parmesan cheese', topn=14))


print('SIMILARITY')

print(model.wv.similarity('egg', 'egg'))
print(model.wv.similarity('mushroom', 'mushrooms'))
print(model.wv.similarity('mushrooms', 'shiitake mushrooms'))
print(model.wv.similarity('tangerines', 'oranges'))
print(model.wv.similarity('lemon juice', 'meat'))
print(model.wv.similarity('honey', 'sugar'))
print(model.wv.similarity('chicken', 'pork'))
print(model.wv.similarity('garlic cloves', 'garlic'))
print(model.wv.similarity('mozzarella cheese', 'cheddar cheese'))
print(model.wv.similarity("bananas", "oranges"))
print(model.wv.similarity("oranges", "butter"))
print(model.wv.similarity("sausages", "lime zest"))


recipe1 = ['spaghetti', 'garlic', 'pepper', 'salt', 'parmigiano', 'tomatoes']
recipe2 = ['flour', 'eggs', 'chocolate', 'cinnamon', 'orange juice']
recipe3  = ['linguine', 'tomatoes', 'salt', 'basil', 'pepper']

print(model.wv.n_similarity(recipe1, recipe2))
print(model.wv.n_similarity(recipe1, recipe3))
print(model.wv.n_similarity(recipe2, recipe3))