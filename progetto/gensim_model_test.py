#PREDICTION

# Compute prediction in the three ways (project paper)

import gensim
from pymongo import MongoClient
import pymongo
import spacy
import copy
import re
import random
import json

# Load pre-trained Word2Vec model.
model = gensim.models.Word2Vec.load("1mln_80k_new_less.model")

with open('clusters.json', 'r') as json_file:
    clusters = json.load(json_file)

host = 'localhost'
port = 27017

myclient = MongoClient(host, port)
db = myclient["recipesdb"]
recipes = db["new_recipes"]

pymongo_cursor = db.recipes.find().skip(200000).limit(500000)

all_r = []

for recipe in pymongo_cursor:
	recipe_ingredients = []
	for recipe_ingredient in recipe['ingredients']:
		recipe_ingredients.append(recipe_ingredient['text'])
	all_r.append(recipe_ingredients)
nlp = spacy.load('models/80k_new_less/model-best')

all_recipes = []

for ingredients in all_r:
	recipe_ingredients = set()
	for ingredient in ingredients:
		ingredient = ingredient.lower()
		# ingredient = re.sub(r"[^a-zA-Z0-9]+", ' ', ingredient).strip()
		# ingredient = re.sub(r'\b\w{1,2}\b', ' ', ingredient).strip()
		# ingredient = ' '.join(ingredient.split())
		if ingredient != '' or ingredient != ' ':
			recipe_ingredients.add(ingredient)
	all_recipes.append(recipe_ingredients)

predict = []

for i, recipe in enumerate(all_recipes):
	test_ingredients = set()
	for ingredient in recipe:
		doc = nlp(ingredient)
		for ent in doc.ents:
			test_ingredients.add(ent.text)
	predict.append(list(test_ingredients))

predict_minus_one = copy.deepcopy(predict)
for p in predict_minus_one:
	try:
		p.pop(random.randrange(len(p)))
	except:
		pass

similarity = []
no_show = []
cluster_values = []

for recipe, r in zip(predict_minus_one, predict):
	try:
		missing = set(r).difference(set(recipe))
		missing = missing.pop()
		predicted_ingredient = model.predict_output_word(recipe, topn=20)
		cluster_missing = clusters.get(missing, -1)
		cluster_recipe = list(map(lambda x: clusters.get(x, -1), recipe))
		cluster_pred = list(map(lambda x: clusters.get(x[0], -1), predicted_ingredient))
		for label, cp in zip(predicted_ingredient, cluster_pred):
			if cp not in cluster_recipe:
				# print(f"FIRST NOT IN: {label[0]} {cp}")
				# print(f"SIMILARITY: {model.wv.similarity(missing, label[0])}")
				cluster_values.append(model.wv.similarity(missing, label[0]))
				break
		# print("RECIPE")
		# print(list(zip(recipe, cluster_recipe)))
		# print("PREDICTED")
		# print(list(zip(predicted_ingredient, cluster_pred)))
		# print("MISSING")
		# print(cluster_missing)
		# print(predicted_ingredient)
		p = model.predict_output_word(recipe, topn=1)[0][0]

		no_show.append(model.wv.similarity(missing, p))
		similar = list(map(lambda x: (x[0], model.wv.similarity(missing, x[0])), predicted_ingredient))
		sorted_similar = sorted(similar, key=lambda tup: tup[1], reverse=True)
		similarity.append(sorted_similar[0][1])

	except KeyError:
		pass
	except TypeError:
		pass
	except ValueError:
		pass

import matplotlib.pyplot


print(f"exact {len(list(filter(lambda x: x > 0.98, similarity)))}")
print(f"0.60 {len(list(filter(lambda x: x > 0.60, similarity)))}")
print(f"0.30 {len(list(filter(lambda x: x > 0.30, similarity)))}")
matplotlib.pyplot.scatter(range(0, len(similarity)), similarity)

matplotlib.pyplot.show()

print(f"exact {len(list(filter(lambda x: x > 0.98, no_show)))}")
print(f"0.60 {len(list(filter(lambda x: x > 0.60, no_show)))}")
print(f"0.31 {len(list(filter(lambda x: x > 0.30, no_show)))}")
matplotlib.pyplot.scatter(range(0, len(no_show)), no_show, color='green')

matplotlib.pyplot.show()

print(f"exact {len(list(filter(lambda x: x > 0.98, cluster_values)))}")
print(f"0.60 {len(list(filter(lambda x: x > 0.60, cluster_values)))}")
print(f"0.31 {len(list(filter(lambda x: x > 0.30, cluster_values)))}")
matplotlib.pyplot.scatter(range(0, len(cluster_values)), cluster_values, color='red')

matplotlib.pyplot.show()