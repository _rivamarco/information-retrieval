# Converter to make labeled-data in spacy train command
# https://spacy.io/api/cli

# spacy train from cli is faster than using code




import spacy
from spacy.gold import biluo_tags_from_offsets
import json
import sys

#def getcharoffsetsfromwordoffsets(doc,entities):
#    charoffsets = []
#    for entity in entities:
#        span = doc[entity[0]:entity[1]]
#        charoffsetentitytuple = (span.start_char, span.end_char, entity[2])
#        charoffsets.append(charoffsetentitytuple)
#    return charoffsets


def convertspacyapiformattocliformat(nlp, TRAIN_DATA):
    docnum = 1
    documents = []
    for t in TRAIN_DATA:
        doc = nlp(t[0])
        #charoffsetstuple = getcharoffsetsfromwordoffsets(doc,t[1]['entities'])
        #tags = biluo_tags_from_offsets(doc, charoffsetstuple)
        tags = biluo_tags_from_offsets(doc, t[1]['entities'])
        ner_info = list(zip(doc, tags))
        tokens = []
        sentences = []
        for n, i in enumerate(ner_info):
            token = {"head" : 0,
            "dep" : "",
            "tag" : "",
            "orth" : i[0].string,
            "ner" : i[1],
            "id" : n}
            tokens.append(token)
        sentences.append({'tokens' : tokens})
        document = {}
        document['id'] = docnum
        docnum+=1
        document['paragraphs'] = []
        paragraph = {'raw': doc.text,"sentences" : sentences}
        document['paragraphs'] = [paragraph]
        documents.append(document)
    return documents

if len(sys.argv) == 2:

    with open(f'./training_data/{sys.argv[1]}_train.json') as json_file:
        TRAIN_DATA = json.load(json_file)

    with open(f'./training_data/{sys.argv[1]}_validation.json') as json_file:
        VALIDATION_DATA = json.load(json_file)

    nlp = spacy.blank("en")

    train = convertspacyapiformattocliformat(nlp, TRAIN_DATA)
    validation = convertspacyapiformattocliformat(nlp, VALIDATION_DATA)


    with open(f'./training_data/converted_{sys.argv[1]}_train.json', 'w') as outfile:
        json.dump(train, outfile)

    with open(f'./training_data/converted_{sys.argv[1]}_validation.json', 'w') as outfile:
        json.dump(validation, outfile)
        
else:
    print('Missing parameters: filename')