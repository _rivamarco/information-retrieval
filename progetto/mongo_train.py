# Build spacy training set from mongodb recipes


import pymongo
import json
import sys
from pymongo import MongoClient
import spacy
import progressbar
import random
from flashtext import KeywordProcessor
import nltk
from nltk.corpus import stopwords

if len(sys.argv) == 3:

	nltk.download('stopwords')

	keyword_processor = KeywordProcessor()
	keyword_processor_replace = KeywordProcessor()
	nlp = spacy.load('en')


	host = 'localhost'
	port = 27017

	myclient = MongoClient(host, port)
	db = myclient["recipesdb"]
	recipes = db["new_recipes"]

	with open('ingredient_train_max_three.json') as json_file:
		ingredients = json.load(json_file)

	print(len(ingredients))

	ingredients = list(filter(lambda x: len(x.split(' ')) <= 3, ingredients))

	print(len(ingredients))

	words = set([line.rstrip('\n') for line in open('google-10000-english.txt')])
	words = words.difference(set(ingredients))
	words = [word for word in words if word not in stopwords.words('english')]

	ingredients_randomized = list(map(lambda x: (x, random.sample(words, 1)[0]), ingredients))

	keyword_processor.add_keywords_from_list(ingredients)
	for tup in ingredients_randomized:
		keyword_processor_replace.add_keyword(*tup)

	TRAIN_DATA = []
	LABEL = 'INGREDIENT'

	bag_of_ingredients = set()
	not_found_bag_of_ingredients = set(ingredients).copy()

	limit = int(sys.argv[1])

	pymongo_cursor = db.recipes.find().skip(500000).limit(limit)

	for recipe in pymongo_cursor:
		for recipe_ingredient in recipe['ingredients']:
			recipe_ingredient = recipe_ingredient['text']
			recipe_ingredient = recipe_ingredient.replace(',', '').lower()
			result = keyword_processor.extract_keywords(recipe_ingredient, span_info=True)
			train_example = (recipe_ingredient, {'entities': []})
			for ing in result:
				name, start, end = ing
				train_example[1]['entities'].append((start, end, LABEL))
				bag_of_ingredients.add(ing[0])
				not_found_bag_of_ingredients.discard(ing[0])
			TRAIN_DATA.append(train_example)

	partition = int(len(TRAIN_DATA) * 0.70)
	VALIDATION_DATA = TRAIN_DATA[partition:len(TRAIN_DATA)]

	TRAIN_DATA_REPLACE = []

	pymongo_cursor.rewind()

	for recipe in pymongo_cursor:
		for recipe_ingredient in recipe['ingredients']:
			recipe_ingredient = recipe_ingredient['text']
			recipe_ingredient = recipe_ingredient.replace(',', '').lower()
			replaced = keyword_processor_replace.replace_keywords(recipe_ingredient)
			result = keyword_processor.extract_keywords(replaced, span_info=True)
			train_example = (replaced, {'entities': []})
			for ing in result:
				name, start, end = ing
				train_example[1]['entities'].append((start, end, LABEL))
			TRAIN_DATA_REPLACE.append(train_example)

	partition_replace = int(len(TRAIN_DATA_REPLACE) * 0.70)
	VALIDATION_DATA_REPLACE = TRAIN_DATA_REPLACE[partition_replace:len(TRAIN_DATA_REPLACE)]

	llimit = int(limit/10000)

	TRAIN_DATA.extend(TRAIN_DATA_REPLACE[:llimit])
	VALIDATION_DATA.extend(VALIDATION_DATA_REPLACE[:llimit])


	with open(f"./training_data/{sys.argv[2]}_train.json", 'w') as outfile:
		json.dump(TRAIN_DATA, outfile, indent=4)

	with open(f'./training_data/{sys.argv[2]}_validation.json', 'w') as outfile:
		json.dump(VALIDATION_DATA, outfile, indent=4)

else:
    print('Missing arguments: n filename')