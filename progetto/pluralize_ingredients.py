# pluralization experiment, not working

import json
from inflection import pluralize
import collections

big_list = []

with open('ingredient_train_max_two.json') as json_file:
	data = json.load(json_file)
print(len(data))
ingredients = list(map(lambda x: pluralize(x), data))
print([item for item, count in collections.Counter(ingredients).items() if count > 1])
big_list.extend(ingredients)
    
ingredients = set(big_list)
print(len(ingredients))

with open('ingredient_train_pluralized_two.json', 'w') as outfile:
    json.dump(list(ingredients), outfile, indent=4)

with open('ingredient_train_max_three.json') as json_file:
	data = json.load(json_file)
print(len(data))
ingredients = list(map(lambda x: pluralize(x), data))
print([item for item, count in collections.Counter(ingredients).items() if count > 1])
big_list.extend(ingredients)
    
ingredients = set(big_list)
print(len(ingredients))

with open('ingredient_train_pluralized_three.json', 'w') as outfile:
    json.dump(list(ingredients), outfile, indent=4)
